## Para rodar o projeto

1 - `git clone (ssh / https do rep)`

2 - Para baixar as dependencias do projeto execute o `yarn install`

3 - `npm install -g json-server` (vai ser utilizado para rodar a fake api)

4 - Abra um terminal a mais e execute `json-server --watch db.json --port 3001`

5 - Execute o `yarn start` para rodar o projeto.
