import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Skeleton from "@material-ui/lab/Skeleton";

const useStyles = makeStyles({
  root: {
    margin: '4em 0 0 3em'
  }
});

export default function Media(props) {
  const classes = useStyles();
  return (
    <Grid wrap="nowrap" container justify="center">
      <Box width={275} margin={0} my={5} className={classes.root}>
        <Skeleton variant="rect" width="60%" />
        <Skeleton width="60%" />
        <Skeleton width="60%" />
        <Skeleton width="60%" />
      </Box>
    </Grid>
  );
}
