import React from "react";
import Slider from "react-slick";
import SimpleCard from "./Cards";
import { makeStyles } from "@material-ui/core/styles";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import "./slideStyles.css";

const useStyles = makeStyles({
  root: {
    width: "95%",
    display: "flex",
    alignItems: "center",
    margin: "0 auto",
  },
  arrows: {
    display: "block",
    color: "white",
    cursor: "pointer",
    background: "gray",
    "&:hover": {
      background: "gray",
      color: "white",
    },
  },
  cards: {
    margin: "0 auto",
  },
});

const SliderEvents = ({ item }) => {
  const classes = useStyles();
  const [data, setData] = React.useState([{}]);
  const [page, setPage] = React.useState(24);
  const [hasError, setErrors] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(null);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  async function fetchData() {
    const res = await fetch(
      `http://localhost:3001/events?start=0&_limit=${page}`
    );
    res
      .json()
      .then((res) => setData(res))
      .catch((err) => setErrors(err));
  }

  React.useEffect(() => {
    setIsLoading(true);
    fetchData();
    setTimeout(() => {
      setIsLoading(false);
    }, 200);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const rows = data && data.length <= 4 ? 1 : 2;

  function SampleNextArrow(props) {
    const { onClick, className } = props;
    const classes = useStyles();
    return (
      <NavigateNextIcon
        className={[className, classes.arrows].join(" ")}
        // onClick={() => setPage(page < 5 ? page + 1 : page)}
        onClick={onClick}
      />
    );
  }

  function SamplePrevArrow(props) {
    const { onClick, className } = props;
    const classes = useStyles();
    return (
      <NavigateBeforeIcon
        className={[className, classes.arrows].join(" ")}
        // onClick={() => setPage(page > 1 ? page - 1 : page)}
        onClick={onClick}
      />
    );
  }

  const settings = {
    infinite: true,
    speed: 500,
    rows: rows,
    arrows: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    afterChange: () => {
      setPage(page + 8);
    },
    responsive: [
      {
        breakpoint: 9999,
        settings: {
          rows: item.rows ? item.rows : 2,
          slidesPerRow: 4,
        },
      },
      {
        breakpoint: 768,
        settings: {
          rows: 1,
          slidesPerRow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          rows: 1,
          slidesPerRow: 1,
        },
      },
    ],
  };

  return (
    <React.Fragment>
      <Typography
        style={{
          display: "flex",
          justifyContent: "center",
          margin: 25,
          color: "blue",
          fontSize: 25,
        }}
      >
        Produtos
      </Typography>
      <Grid className={classes.root}>
        {/* <SamplePrevArrow /> */}
        <Grid item xs={8} className={classes.cards}>
          <Slider {...settings}>
            {data.map((item) => (
              <Grid key={item.id}>
                <SimpleCard
                  name={item.name}
                  modality={item.modality}
                  isLoading={isLoading}
                />
              </Grid>
            ))}
          </Slider>
        </Grid>
        {/* <SampleNextArrow /> */}
      </Grid>
    </React.Fragment>
  );
};

export default SliderEvents;
