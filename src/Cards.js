import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import CardsSkeleton from "./CardsSkeleton";

const useStyles = makeStyles(({}) => {
  return {
    card: {
      width: 170,
      height: 181,
      margin: 5,
      borderRadius: 5,
      boxShadow:
        "0px 2px 1px -1px rgba(0,0,0,1.2), 0px 1px 1px 0px rgba(0,0,0,1.14), 0px 1px 3px 0px rgba(0,0,0,0.12)",
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
      backgroundColor: "rgb(236, 102, 11)",
    },
    head: {
      height: "10px",
      backgroundColor: "rgb(236, 102, 11)",
    },
  };
});

export default function SimpleCard({ name, modality, isLoading }) {
  const classes = useStyles();
  return (
    <Card className={classes.card}>
      {isLoading ? (
        <React.Fragment>
          <CardsSkeleton />
        </React.Fragment>
      ) : (
        <Grid>
          <CardContent>
            <Grid className={classes.head} />
            <Typography variant="h5" component="h2" align="center">
              {name}
            </Typography>
            <Typography
              className={classes.pos}
              color="textSecondary"
              align="center"
            >
              Pregão ao vivo
            </Typography>
            <Typography variant="body2" component="p" align="center">
              {modality}
            </Typography>
            <Typography variant="span" component="p" align="center">
              Veículos de Passeio e Utilitários
            </Typography>
          </CardContent>
        </Grid>
      )}
    </Card>
  );
}
