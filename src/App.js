import React from "react";
import SliderEvents from "./SliderEvents";
import SliderProducts from "./SliderProducts";
import SliderStores from "./SliderStores";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import config from "./config.json";
import LazyLoad from "react-lazyload";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  header: {
    textAlign: "center",
    margin: "0",
    padding: "10px",
    fontSize: "21px",
  },
});

const App = () => {
  const classes = useStyles();

  const Row = ({ type, item }) => {
    if (type === "events") {
      return <SliderEvents item={item} />;
    } else if (type === "offers") {
      return <SliderProducts item={item} />;
    } else if (type === "stores") {
      return <SliderStores item={item} />;
    } else {
      return "";
    }
  };

  return (
    <Grid item xs={12} className={classes.root}>
      <header className={classes.header}>
        <h3>POC Carousel</h3>
      </header>
      {config.segmentos.map((item, index) => (
        <LazyLoad height={window.innerHeight}>
          {item.shelf.map((segment, index) => (
            <Row type={segment.type} item={item} />
          ))}
        </LazyLoad>
      ))}
    </Grid>
  );
};

export default App;
